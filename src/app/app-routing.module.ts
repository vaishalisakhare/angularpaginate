import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { UserdataComponent } from './userdata/userdata.component';
import { NgxPaginationModule } from 'ngx-pagination';

const routes: Routes = [
  { path: '' , component: FormComponent },
  { path: 'userdata' , component: UserdataComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    NgxPaginationModule
  ],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppRoutingModule { }
