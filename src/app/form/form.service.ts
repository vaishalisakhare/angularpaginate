import { Injectable } from '@angular/core';
import { catchError, map, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  apiurl = 'https://reqres.in/api/users';
  constructor(private http: HttpClient) { }
  getUserDetails() {
    return this.http.get(this.apiurl).pipe(map((res) => res),
      catchError((err: HttpErrorResponse) => {
        if (err.status === 0) {
          const msg = 'Please Check Internet Connection';
          return throwError(err.status || msg);
        } else {
          const msg = 'server response error';
          return throwError(err.status || msg);
        }

      })
    );
  }
}
