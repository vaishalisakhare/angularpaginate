import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormService } from './form.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  form: any = FormGroup;
  userData: any;
  isSubmitted = false;
  email = 'admin@gmail.com';
  password = 'admin@123';
  constructor(private formbuilder: FormBuilder, private service: FormService,
              private router: Router) { 
    this.validateLogin(this.form);
  }
  
  ngOnInit(): void {
    this.form = this.formbuilder.group({
      emailId : [null, Validators.compose([Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)])],
      Password: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12)])]
    })
  }
  get errorControl() {
    return this.form.controls;
  }
  validateLogin(form: any) {
    this.isSubmitted = true;
    if(form.emailId == this.email && form.Password == this.password) {
      this.router.navigate(['/userdata']);
      console.log('userdata page');
    } else {
      console.log('Incorrect Details');
    }
  }
  
}
