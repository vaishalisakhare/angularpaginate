import { Component, OnInit } from '@angular/core';
import { FormService } from '../form/form.service';

@Component({
  selector: 'app-userdata',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.css']
})
export class UserdataComponent implements OnInit {
  userData: any;
  config: any;
  cp: number = 1;
  page: any = 2;
  limit:any = 12;
  constructor(private service: FormService) {
    this.getUserDetails();

   }

  ngOnInit(): void {
  }
getUserDetails() {
  this.service.getUserDetails().subscribe(data => {
    this.userData = data;
    console.log(this.userData);
  });
}
pageChanged(event: any){
  this.page = event;

}
editUserData(id: any) {
  console.log('edited id' + id);
}
}
